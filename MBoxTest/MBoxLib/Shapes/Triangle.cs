﻿using System;
using static System.Math;

namespace MBoxLib.Shapes
{
    /// <summary>
    /// Треугольник, у которого почему-то нет вершин, но есть длины сторон.
    /// </summary>
    public sealed class Triangle : IShape
    {
        private const double Tolerance = 1e-5;

        public double Length1 { get; }
        public double Length2 { get; }
        public double Length3 { get; }

        public double Area
        {
            get
            {
                // По формуле Герона.
                var p = (Length1 + Length2 + Length3) / 2.0;
                return Sqrt(p * (p - Length1) * (p - Length2) * (p - Length3));
            }
        }

        public Triangle(double length1, double length2, double length3)
        {
            if (length1 < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length1));
            }

            if (length2 < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length1));
            }

            if (length3 < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length1));
            }

            Length1 = length1;
            Length2 = length2;
            Length3 = length3;
        }

        /// <summary>
        /// Создает треугольник по трём вершинам.
        /// </summary>
        /// <param name="point1">Вершина 1.</param>
        /// <param name="point2">Вершина 2.</param>
        /// <param name="point3">Вершина 3.</param>
        /// <returns>Треугольник.</returns>
        public static Triangle FromVertices((int x, int y) point1, (int x, int y) point2, (int x, int y) point3)
        {
            var length1 = Sqrt(Sqr(point1.x - point2.x) + Sqr(point1.y - point2.y));
            var length2 = Sqrt(Sqr(point2.x - point3.x) + Sqr(point2.y - point3.y));
            var length3 = Sqrt(Sqr(point1.x - point3.x) + Sqr(point1.y - point3.y));

            return new Triangle(length1, length2, length3);
        }

        private static double Sqr(double value)
        {
            return Pow(value, 2.0);
        }

        /// <summary>
        /// Проверяет, является ли треугольник прямоугольным.
        /// </summary>
        /// <returns><c>True</c>, если является.</returns>
        public bool IsRightTriangle()
        {
            return Abs(Length1 - Sqrt(Sqr(Length2) + Sqr(Length3))) < Tolerance
                   || Abs(Length2 - Sqrt(Sqr(Length1) + Sqr(Length3))) < Tolerance
                   || Abs(Length3 - Sqrt(Sqr(Length1) + Sqr(Length2))) < Tolerance;
        }
    }
}