using System;
using MBoxLib.Shapes;
using NUnit.Framework;

namespace MBoxLib.Tests
{
    public class ShapesTest
    {
        [Test]
        public void TestCircleArea()
        {
            var shape = new Circle(2);
            Assert.AreEqual(shape.Area, 12.566, 0.001);
            
            shape = new Circle(0);
            Assert.AreEqual(shape.Area, 0.0, 0.001);
        }

        [Test]
        public void TestCircleThrows()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { new Circle(-1); });
        }

        [Test]
        public void TestTriangleThrows()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { new Triangle(-1, -1, -1); });
        }

        [Test]
        public void TestTriangle()
        {
            var shape = Triangle.FromVertices((0, 0), (1, 0), (1, 1));
            Assert.AreEqual(shape.Area, 0.5, 0.001);
            Assert.IsTrue(shape.IsRightTriangle());

            shape = new Triangle(1.0, 1.0, 1.0);
            Assert.AreEqual(shape.Area, .433, 0.001);
        }
    }
}