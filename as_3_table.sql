-- Реализация с нормализацией в 3 таблицы.
-- Подойдет, если нужно управлять тегами и добавлять в них дополнительную информацию или выстроить их иерархию.

drop table news_tags_map;
drop table news;
drop table tags;


CREATE TABLE news
(
    news_id    UNIQUEIDENTIFIER DEFAULT newid() NOT NULL
        CONSTRAINT news_pk
            PRIMARY KEY NONCLUSTERED,
    title      NVARCHAR(150)                    NOT NULL,
    body       NVARCHAR(MAX),
    created_on DATETIME2        DEFAULT getdate()
)
GO

CREATE TABLE tags
(
    tag_id UNIQUEIDENTIFIER DEFAULT NEWID()
        CONSTRAINT TAGS_PK
            PRIMARY KEY NONCLUSTERED,
    tag_name NVARCHAR(50) NOT NULL
)
GO

create table news_tags_map
(
    id UNIQUEIDENTIFIER default NEWID()
        CONSTRAINT news_tags_map_pk
            PRIMARY KEY NONCLUSTERED,
    news_id UNIQUEIDENTIFIER
        CONSTRAINT news_tags_fk_news_id
            REFERENCES news (news_id)
                ON UPDATE CASCADE ON DELETE CASCADE,
    tag_id UNIQUEIDENTIFIER
        CONSTRAINT news_tags_fk_tags_id
            REFERENCES tags (tag_id)
                ON UPDATE CASCADE ON DELETE CASCADE
)
GO

INSERT INTO news (news_id, title, body, created_on) VALUES 
('AD5EF9C7-CFC1-4765-A19E-7D110496D1C0', N'В Саратове прекратили производство холодильников «Саратов»', N'Саратовское электроагрегатное производственное объединение (СЭПО) прекратило выпуск своего главное продукта — холодильников «Саратов».', '2020-02-20 12:23:38.3233333'),
('D7763868-248E-4E76-AF67-1DCD90A26C19', N'Как спрятаться от полицейских дронов? Мастер-класс от победителя «Танцев на ТНТ» (это пластический этюд!)', N'«Болит.Балет» — это театральный ютьюб-проект, над которым работают креативный продюсер «Газгольдера» Сергей Ильин и хореограф и актер «Гоголь-центра» Игорь Шаройко. Они ставят пластические этюды по мотивам актуальных российских новостей.', '2020-02-20 12:25:09.9066667'),
('21631990-6455-4F1D-9F9B-276151C6F8EC', N'Зачем металлургическому предприятию штатный VR-педагог?', N'Металлургия — важный промышленный сектор. Хотя непосредственно на нее приходится лишь 5% ВВП России, она обеспечивает работу еще трех ключевых индустрий: промышленности, строительства и топливно-энергетического комплекса. А это уже 58% ВВП! В металлургии работают не только шахтеры, литейщики и специалисты по обработке сплавов. Вместе с компанией «Северсталь» рассказываем про восемь профессий, которые сейчас востребованы в металлургии.', '2020-02-20 12:27:43.1333333');

INSERT INTO tags (tag_id, tag_name) VALUES
('7377C868-6ABE-4307-BADA-EE99E24CAA5F', N'Техника'),
('60A4BA32-AF9E-43F8-84C9-84AEEB5A062C', N'VR'),
('55146B7D-6964-4613-9253-EA4F5F9A25B6', N'Саратов');

INSERT INTO news_tags_map (id, news_id, tag_id) VALUES
('09153D4C-BCEB-41DE-8D87-488214CDB006', 'AD5EF9C7-CFC1-4765-A19E-7D110496D1C0', '7377C868-6ABE-4307-BADA-EE99E24CAA5F'),
('5B8994BB-A497-41EF-9704-AFA903E5C60D', '21631990-6455-4F1D-9F9B-276151C6F8EC', '7377C868-6ABE-4307-BADA-EE99E24CAA5F'),
('8F11498D-B0AF-4B2E-B98A-051A282FD46D', 'AD5EF9C7-CFC1-4765-A19E-7D110496D1C0', '55146B7D-6964-4613-9253-EA4F5F9A25B6');


select n.title, n.body, t.tag_name
from news n
left join news_tags_map ntm ON n.news_id = ntm.news_id
left join tags t ON ntm.tag_id = t.tag_id
