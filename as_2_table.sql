-- Реализация в 2 таблицы с денормализацией.
-- Подойдет, если просто нужно хранить теги для каждой новости и иногда делать поиск новости по тегу.

drop table tags;
drop table news;

CREATE TABLE news
(
    news_id    UNIQUEIDENTIFIER DEFAULT newid() NOT NULL
        CONSTRAINT news_pk
            PRIMARY KEY NONCLUSTERED,
    title      NVARCHAR(150)                    NOT NULL,
    body       NVARCHAR(MAX),
    created_on DATETIME2        DEFAULT getdate()
)
GO

CREATE TABLE tags
(
    tag_id UNIQUEIDENTIFIER DEFAULT NEWID()
        CONSTRAINT TAGS_PK
            PRIMARY KEY NONCLUSTERED,
    tag_name NVARCHAR(50) NOT NULL,
    news_id  UNIQUEIDENTIFIER
        CONSTRAINT news_tags_news_id_fk
            REFERENCES news
            ON UPDATE CASCADE ON DELETE CASCADE
)
GO

INSERT INTO news (news_id, title, body, created_on) VALUES 
('AD5EF9C7-CFC1-4765-A19E-7D110496D1C0', N'В Саратове прекратили производство холодильников «Саратов»', N'Саратовское электроагрегатное производственное объединение (СЭПО) прекратило выпуск своего главное продукта — холодильников «Саратов».', '2020-02-20 12:23:38.3233333'),
('D7763868-248E-4E76-AF67-1DCD90A26C19', N'Как спрятаться от полицейских дронов? Мастер-класс от победителя «Танцев на ТНТ» (это пластический этюд!)', N'«Болит.Балет» — это театральный ютьюб-проект, над которым работают креативный продюсер «Газгольдера» Сергей Ильин и хореограф и актер «Гоголь-центра» Игорь Шаройко. Они ставят пластические этюды по мотивам актуальных российских новостей.', '2020-02-20 12:25:09.9066667'),
('21631990-6455-4F1D-9F9B-276151C6F8EC', N'Зачем металлургическому предприятию штатный VR-педагог?', N'Металлургия — важный промышленный сектор. Хотя непосредственно на нее приходится лишь 5% ВВП России, она обеспечивает работу еще трех ключевых индустрий: промышленности, строительства и топливно-энергетического комплекса. А это уже 58% ВВП! В металлургии работают не только шахтеры, литейщики и специалисты по обработке сплавов. Вместе с компанией «Северсталь» рассказываем про восемь профессий, которые сейчас востребованы в металлургии.', '2020-02-20 12:27:43.1333333');

INSERT INTO tags (tag_id, news_id, tag_name) VALUES
('09153D4C-BCEB-41DE-8D87-488214CDB006', 'AD5EF9C7-CFC1-4765-A19E-7D110496D1C0', N'Техника'),
('5B8994BB-A497-41EF-9704-AFA903E5C60D', '21631990-6455-4F1D-9F9B-276151C6F8EC', N'Техника'),
('8F11498D-B0AF-4B2E-B98A-051A282FD46D', 'AD5EF9C7-CFC1-4765-A19E-7D110496D1C0', N'Саратов');

select n.title, n.body, t.tag_name
from news n
left join tags t ON t.news_id = n.news_id
