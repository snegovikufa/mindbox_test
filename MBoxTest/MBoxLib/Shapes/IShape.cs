﻿namespace MBoxLib.Shapes
{
    /// <summary>
    /// Базовый интерфейс для фигур.
    /// </summary>
    public interface IShape
    {
        /// <summary>
        /// Площадь фигуры.
        /// </summary>
        double Area { get; }
    }
}