﻿using System;

namespace MBoxLib.Shapes
{
    /// <summary>
    /// Окружность.
    /// </summary>
    public sealed class Circle : IShape
    {
        /// <summary>
        /// Радиус.
        /// </summary>
        public int Radius { get; }

        public double Area => Math.PI * Math.Pow(Radius, 2);

        public Circle(int radius)
        {
            if (radius < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(radius), radius, "Circle radius should be greater than zero");
            }

            Radius = radius;
        }
    }
}